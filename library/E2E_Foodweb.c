/* file E2E_Foodweb.c*/

#include <stdio.h>

#include <R.h>

#include <Rmath.h>

#include <Rdefines.h>

#include <Rinternals.h>

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

//        FIRST DEFINE SOME FUNCTIONS THAT ARE GENERIC THROUGHOUT THE MODEL
//------------------------------------------------------------------------

/* _____Function to find minimum of two numbers_____*/
// Function adapted from a maxValue function on the interweb, void? - void would mean it couldn't be called on again, so no.

double twomin(double x, double y)
  {
     double min = x;		// make first value the minimum
     {
          if(y < min)
                min = y;	// if second value is smaller than min, overwrite min
     }
     return min;                // return smallest value
}


//------------------------------------------------------------------------

/* _____Heterotrophic uptake functions_____*/
double  f1(double a,double b,double k1,double k2)
{
  
 return ((b*k1*a)/(a+k2));
  
}

//------------------------------------------------------------------------

/* _____Autotrophic uptake functions_____*/

double  f2(double a, double b, double c, double k1, double k2, double k3)
{
  double x = (c/k3);
  double minimum = twomin(1, x);
  return ((minimum * (b*k1*a))/(a+k2));
 
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

//    SET UP THE INTERFACE BETWEEN THE C-MODEL AND THE SURROUNDING R CODE
//------------------------------------------------------------------------



/* _____Define Parameters_____ */
static double parms[120]; //declare empty vector - will be filled by ode function

#define thik_s parms[0]
#define thik_d parms[1]
#define thik_x parms[2]
#define porosity parms[3]
#define thik_b parms[4]
#define Lmaxup parms[5]
#define sed_wat_dif parms[6]
#define qtena parms[7]
#define qtenh parms[8]
#define qtenm parms[9]
#define qtenr parms[10]
#define p_pref parms[11]
#define d_pref parms[12]
#define uNIT_phytt parms[13]
#define hsNIT_phyt parms[14] 
#define uAMM_phytt parms[15]
#define hsAMM_phyt parms[16]
#define uphyt_herbt parms[17]
#define hsphyt_herb parms[18]
#define udet_herbt parms[19]
#define hsdet_herb parms[20]
#define uherb_carnt parms[21]
#define hsherb_carn parms[22]
#define ufishplar_carnt parms[23]
#define hsfishplar_carn parms[24]
#define ufishdlar_carnt parms[25]
#define hsfishdlar_carn parms[26]
#define uherb_fishplart parms[27]
#define hsherb_fishplar parms[28]
#define uherb_fishpt parms[29]
#define hsherb_fishp parms[30]
#define ucarn_fishpt parms[31]
#define hscarn_fishp parms[32]
#define ufishdlar_fishpt parms[33]
#define hsfishdlar_fishp parms[34]
#define ufishplar_fishpt parms[35]
#define hsfishplar_fishp parms[36]
#define uherb_fishdlart parms[37]
#define hsherb_fishdlar parms[38]
#define ucarn_fishdt parms[39]
#define hscarn_fishd parms[40]
#define ubenths_fishdt parms[41]
#define hsbenths_fishd parms[42]
#define ubenthc_fishdt parms[43]
#define hsbenthc_fishd parms[44]
#define ufishplar_fishdt parms[45]
#define hsfishplar_fishd parms[46]
#define ufishdlar_fishdt parms[47]
#define hsfishdlar_fishd parms[48]
#define ufishp_fishdt parms[49]
#define hsfishp_fishd parms[50]
#define ufishd_fishdt parms[51]
#define hsfishd_fishd parms[52]
#define udisc_fishdt parms[53]
#define hsdisc_fishd parms[54]
#define ucorp_fishdt parms[55]
#define hscorp_fishd parms[56]
#define uphyt_benthst parms[57]
#define hsphyt_benths parms[58]
#define udet_benthst parms[59]
#define hsdet_benths parms[60]
#define used_benthst parms[61]
#define hssed_benths parms[62]
#define ubenths_benthct parms[63]
#define hsbenths_benthc parms[64]
#define ucorp_benthct parms[65]
#define hscorp_benthc parms[66]
#define ufishp_bird parms[67]
#define hsfishp_bird parms[68]
#define ufishd_bird parms[69]
#define hsfishd_bird parms[70]
#define udisc_bird parms[71]
#define hsdisc_bird parms[72]
#define ucorp_bird parms[73]
#define hscorp_bird parms[74]
#define aH parms[75]
#define aC parms[76]
#define aBs parms[77]
#define aBc parms[78]
#define aFplar parms[79]
#define aFdlar parms[80]
#define aFp parms[81]
#define aFd parms[82]
#define abird parms[83]
#define eHt parms[84]
#define eCt parms[85]
#define eBst parms[86]
#define eBct parms[87]
#define eFplart parms[88]
#define eFdlart parms[89]
#define eFpt parms[90]
#define eFdt parms[91]
#define ebird parms[92]
#define mt parms[93]
#define nst parms[94]
#define dst parms[95]
#define ndt parms[96]
#define ddt parms[97]
#define msedt parms[98]
#define nsedt parms[99]
#define dsedt parms[100]
#define xst parms[101]
#define xdt parms[102]
#define xcarn parms[103]
#define xbenthc parms[104]
#define xpfishlar parms[105]
#define xdfishlar parms[106]
#define xpfish parms[107]
#define xdfish parms[108]
#define xbird parms[109]
#define dsink_s parms[110]
#define dsink_d_Klow parms[111]
#define dsink_d_Khi parms[112]
#define Pdiscard parms[113]
#define Sdiscard parms[114]
#define dfdp parms[115]
#define bendamage parms[116]
#define demdamage parms[117]
#define disc_corp parms[118]
#define xcorp_det parms[119]


/* _____initializer_____ */
// Initialises parms with vector from R and passes them to solver via odec function

void odec(void(* odeparms)(int *, double *))
{
    int N= 120; // length of parms
    odeparms(&N, parms);
}

//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------

/* _____DRIVING VARIABLES_____ */
// defined as global vectors. These need to be passed to the solver function from R as additional arguments (...).
// Thus interpolation is completed before model is run, not concurrently.
// If these don't pass to model automatically as global variables then just add them to model input list.
// Drivers are indeed global, but function requires pointers to them.

//static int days=(nyears*360);

static double forc[32]; // passed from ode function


#define driverstemp forc[0]

#define driverdtemp forc[1]

#define driverlight forc[2]

#define driverv_dif forc[3]

#define driverboundsdet forc[4]

#define drivers_inflow forc[5]

#define driverboundddet forc[6]

#define driverd_inflow forc[7]

#define driverboundsamm forc[8]

#define driverbounddamm forc[9]

#define driverboundsnit forc[10]

#define driverbounddnit forc[11]

#define driverboundsphyt forc[12]

#define driverbounddphyt forc[13]

#define drivers_outflow forc[14]

#define driverriver forc[15]

#define driverd_outflow forc[16]

#define drivers_upwell forc[17]

#define driverboundriv_amm forc[18]

#define driverboundriv_nit forc[19]

#define driverboundriv_det forc[20]

#define driveratm_amm forc[21]

#define driveratm_nit forc[22]

#define driverdfish_F forc[23]

#define driverpfish_F forc[24]

#define driversbfish_F forc[25]

#define drivercbfish_F forc[26]

#define driverdfish_discmult forc[27]

#define driverpfish_sp forc[28]

#define driverpfish_rec forc[29]

#define driverdfish_sp forc[30]

#define driverdfish_rec forc[31]

/* _____forcing function initializer_____ */
// Initialises drivers with list from R and passes them to solver via forcc function

void forcc (void (* odeforcs)(int *, double *))
{
  int N=32;
  odeforcs(&N, forc);
}


//------------------------------------------------------------------------
//------------------------------------------------------------------------
//------------------------------------------------------------------------


/* _____DEFINE DYNAMIC VARIABLES OUTSIDE OF MODEL AS STATIC DOUBLES_____ */
// this gives them a dedicated "space" on the memory avoiding using the stack. 
// values of these variables will change iteratively within the model.


static double uNIT_phyt;
static double uAMM_phyt;

static double udet_herb;
static double uphyt_herb;
static double uherb_carn;
static double ufishplar_carn;
static double ufishdlar_carn;
static double uherb_fishplar;
static double uherb_fishp;
static double ucarn_fishp;
static double ufishdlar_fishp;
static double ufishplar_fishp;
static double uherb_fishdlar;

static double ucarn_fishd;
static double ubenths_fishd;
static double ubenthc_fishd;
static double ufishplar_fishd;
static double ufishdlar_fishd;
static double ufishp_fishd;
static double ufishd_fishd;
static double udisc_fishd;
static double ucorp_fishd;
static double uphyt_benths;
static double udet_benths;
static double used_benths;
static double ubenths_benthc;
static double ucorp_benthc;

static double eH;
static double eC;
static double eFp;
static double eFplar;
static double eFdlar;

static double m_s;
static double n_s;
static double d_s;
static double xs;

static double eBs;
static double eBc;
static double eFd;
static double m_d;
static double n_d;
static double d_d;
static double msed;
static double nsed;
static double dsed;
static double xd;
static double corp_det;

static double Fddaily;
static double Fpdaily;
static double Fsbdaily;
static double Fcbdaily;

static double Ddiscard;

static double prop_herb_surf;
static double prop_herb_deep;

static double Upt_samm_sphyt;
static double Upt_snit_sphyt;
static double Upt_sdetritus_herb;
static double Upt_ddetritus_herb;
static double Upt_sphyt_herb;
static double Upt_dphyt_herb;
static double Upt_herb_carn;
static double Upt_fishplar_carn;
static double Upt_fishdlar_carn;
static double Upt_ddetritus_benths;
static double Upt_xdetritus_benths;
static double Upt_dphyt_benths;
static double Upt_corpse_benthc;
static double Upt_benths_benthc;
static double Upt_herb_fishplar;
static double Upt_herb_fishp;
static double Upt_carn_fishp;
static double Upt_fishplar_fishp;
static double Upt_fishdlar_fishp;
static double Upt_herb_fishdlar;
static double Upt_corpse_fishd;
static double Upt_disc_fishd;
static double Upt_carn_fishd;
static double Upt_benths_fishd;
static double Upt_benthc_fishd;
static double Upt_fishplar_fishd;
static double Upt_fishdlar_fishd;
static double Upt_fishp_fishd;
static double Upt_fishd_fishd;
static double Upt_corpse_bird;
static double Upt_disc_bird;
static double Upt_fishp_bird;
static double Upt_fishd_bird;

static double ExcrS_herb;
static double ExcrD_herb;
static double ExcrS_carn;
static double ExcrD_carn;
static double ExcrD_benths;
static double ExcrD_benthc;
static double ExcrS_fishplar;
static double ExcrD_fishplar;
static double ExcrS_fishp;
static double ExcrD_fishp;
static double ExcrS_fishdlar;
static double ExcrD_fishdlar;
static double ExcrD_fishd;
static double ExcrS_bird;

static double Defec_herb;
static double Defec_carn;
static double Defec_benths;
static double Defec_benthc;
static double Defec_fishplar;
static double Defec_fishp;
static double Defec_fishdlar;
static double Defec_fishd;
static double Defec_bird;

static double Assim_herb;
static double Assim_carn;
static double Assim_benths;
static double Assim_benthc;
static double Assim_fishplar;
static double Assim_fishp;
static double Assim_fishdlar;
static double Assim_fishd;
static double Assim_bird;

static double Vmix_detritus;
static double Vmix_ammonia;
static double Vmix_nitrate;
static double Vmix_phyt;

static double detr_settle;

static double s_w_amm_flx;
static double s_w_nit_flx;

static double OceanIN_sdetritus;
static double OceanIN_ddetritus;
static double OceanIN_sammonia;
static double OceanIN_dammonia;
static double OceanIN_snitrate;
static double OceanIN_dnitrate;
static double OceanIN_sphyt;
static double OceanIN_dphyt;

static double OceanOUT_sdetritus;
static double OceanOUT_ddetritus;
static double OceanOUT_sammonia;
static double OceanOUT_dammonia;
static double OceanOUT_snitrate;
static double OceanOUT_dnitrate;
static double OceanOUT_sphyt;
static double OceanOUT_dphyt;

static double Upwelling_det;
static double Upwelling_amm;
static double Upwelling_nit;
static double Upwelling_phyt;

static double Riv_amm_IN;
static double Riv_nit_IN;
static double Riv_det_IN;

static double Atm_amm_IN;
static double Atm_nit_IN;



//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________
//___________________________________________________________________________________________________


/* __________THE MODEL__________ */
//model passed to solver via derivsc function.

void derivsc (int *neq,
	     double *t, 
	     double *y, 
	     double *ydot, 
	     double *yout, 
	     int *ip)
{


/* _____CALCULATE DYNAMIC VARIABLES _____ */

/* _____Apply the appropriate q10 and surface temperature to the uptake and metabolic parameters_____ */

/* _____Autotrophic uptake parameters at surface temperatures_____ */

 uNIT_phyt=	  (exp((((driverstemp -qtenr)*log(qtena))/10)+log(uNIT_phytt)));
 uAMM_phyt=       (exp((((driverstemp -qtenr)*log(qtena))/10)+log(uAMM_phytt)));

/* _____Heterotrophic uptake parameters wc average temperatures_____ */

 udet_herb=       (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(udet_herbt)));
 uphyt_herb=      (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(uphyt_herbt)));
 uherb_carn=      (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(uherb_carnt)));
 ufishplar_carn=  (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(ufishplar_carnt)));
 ufishdlar_carn=  (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(ufishdlar_carnt)));
 uherb_fishplar=  (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(uherb_fishplart)));
 uherb_fishp=     (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(uherb_fishpt)));
 ucarn_fishp=     (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(ucarn_fishpt)));
 ufishdlar_fishp= (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(ufishdlar_fishpt)));
 ufishplar_fishp= (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(ufishplar_fishpt)));
 uherb_fishdlar=  (exp(((((((driverstemp*thik_s)+(driverdtemp*thik_d))/(thik_s+thik_d))-qtenr)*log(qtenh))/10)+log(uherb_fishdlart)));

/* _____Heterotrophic uptake parameters at deep temperatures_____ */

 ucarn_fishd=       (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ucarn_fishdt)));
 ubenths_fishd=     (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ubenths_fishdt)));
 ubenthc_fishd=     (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ubenthc_fishdt)));
 ufishplar_fishd=   (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ufishplar_fishdt)));
 ufishdlar_fishd=   (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ufishdlar_fishdt)));
 ufishp_fishd=      (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ufishp_fishdt)));
 ufishd_fishd=      (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ufishd_fishdt)));
 udisc_fishd=       (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(udisc_fishdt)));
 ucorp_fishd=       (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ucorp_fishdt)));
 uphyt_benths=      (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(uphyt_benthst)));
 udet_benths=       (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(udet_benthst)));
 used_benths=       (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(used_benthst)));
 ubenths_benthc=    (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ubenths_benthct)));
 ucorp_benthc=      (exp((((driverdtemp-qtenr)*log(qtenh))/10)+log(ucorp_benthct)));

/* _____Metabolic and biogeochemical parameters in wc average temperature_____ */

 eH=                (exp((((((driverstemp*thik_s+driverdtemp*thik_d)/(thik_s+thik_d))-qtenr)*log(qtenm))/10)+log(eHt)));
 eC=                (exp((((((driverstemp*thik_s+driverdtemp*thik_d)/(thik_s+thik_d))-qtenr)*log(qtenm))/10)+log(eCt)));
 eFp=               (exp((((((driverstemp*thik_s+driverdtemp*thik_d)/(thik_s+thik_d))-qtenr)*log(qtenm))/10)+log(eFpt)));
 eFplar=            (exp((((((driverstemp*thik_s+driverdtemp*thik_d)/(thik_s+thik_d))-qtenr)*log(qtenm))/10)+log(eFplart)));
 eFdlar=            (exp((((((driverstemp*thik_s+driverdtemp*thik_d)/(thik_s+thik_d))-qtenr)*log(qtenm))/10)+log(eFdlart)));

/* _____Metabolic and biogeochemical parameters at surface temperature_____ */

 m_s=               (exp((((driverstemp-qtenr)*log(qtenm))/10)+log(mt)));
 n_s=               (exp((((driverstemp-qtenr)*log(qtenm))/10)+log(nst)));
 d_s=               (exp((((driverstemp-qtenr)*log(qtenm))/10)+log(dst)));
 xs=               (exp((((driverstemp-qtenr)*log(qtenm))/10)+log(xst)));

/* _____Metabolic and biogeochemical parameters at deep temperature_____*/

 eBs=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(eBst)));
 eBc=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(eBct)));
 eFd=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(eFdt)));
 m_d=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(mt)));
 n_d=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(ndt)));
 d_d=               (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(ddt)));
 msed=              (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(msedt)));
 nsed=              (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(nsedt)));
 dsed=              (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(dsedt)));
 xd=                (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(xdt)));
 corp_det=          (exp((((driverdtemp-qtenr)*log(qtenm))/10)+log(xcorp_det)));

/* _____Interpolate current fishing rate at time t_____ */

 Fddaily=   (driverdfish_F);
 Fpdaily=   (driverpfish_F);
 Fsbdaily=  (driversbfish_F);
 Fcbdaily=  (drivercbfish_F);

/* _____Interpolate the current discard rate for demersal fish_____ */

 Ddiscard=   (twomin(1.0, (exp(-y[19]*dfdp))*driverdfish_discmult));

/* _____Now start calcualating the various flux terms_____ */

/* _____Vertical distribution of zooplankton_____ */

 prop_herb_surf=         ((d_pref*y[0])+(p_pref*y[11]))/((d_pref*y[0])+(d_pref*y[1])+(p_pref*y[11])+(p_pref*y[12]));
// Rprintf("prop_herb_surf=%f\n", prop_herb_surf);
 prop_herb_deep=         ((d_pref*y[1])+(p_pref*y[12]))/((d_pref*y[0])+(d_pref*y[1])+(p_pref*y[11])+(p_pref*y[12]));
// Rprintf("prop_herb_deep=%f\n", prop_herb_deep);
    
/* _____Uptake rates_____*/

 Upt_samm_sphyt=         (f2(y[5],y[11],driverlight,uAMM_phyt,(hsAMM_phyt*thik_s),Lmaxup));
 Upt_snit_sphyt=         (f2(y[8],y[11],driverlight,uNIT_phyt,(hsNIT_phyt*thik_s),Lmaxup));
 Upt_sdetritus_herb=     (f1(y[0],(y[13]*prop_herb_surf),udet_herb,(hsdet_herb*thik_s)));
 Upt_ddetritus_herb=     (f1(y[1],(y[13]*prop_herb_deep),udet_herb,(hsdet_herb*thik_d)));
 Upt_sphyt_herb=         (f1(y[11],(y[13]*prop_herb_surf),uphyt_herb,(hsphyt_herb*thik_s)));
 Upt_dphyt_herb=         (f1(y[12],(y[13]*prop_herb_deep),uphyt_herb,(hsphyt_herb*thik_d)));
 Upt_herb_carn=          (f1(y[13],y[14],uherb_carn,(hsherb_carn*(thik_s+thik_d))));
 Upt_fishplar_carn=      (f1(y[18],y[14],ufishplar_carn,(hsfishplar_carn*(thik_s+thik_d))));
 Upt_fishdlar_carn=      (f1(y[20],y[14],ufishdlar_carn,(hsfishdlar_carn*(thik_s+thik_d))));
 Upt_ddetritus_benths=   (f1((y[1]*(thik_b/thik_d)),y[15],udet_benths,hsdet_benths));
 Upt_xdetritus_benths=   (f1(y[2],y[15],used_benths,hssed_benths));
 Upt_dphyt_benths=       (f1((y[12]*(thik_b/thik_d)),y[15],uphyt_benths,hsphyt_benths));
 Upt_corpse_benthc=      (f1(y[4],y[16],ucorp_benthc,hscorp_benthc));
 Upt_benths_benthc=      (f1(y[15],y[16],ubenths_benthc,hsbenths_benthc));
 Upt_herb_fishplar=      (f1(y[13],y[18],uherb_fishplar,(hsherb_fishplar*(thik_s+thik_d))));
 Upt_herb_fishp=         (f1(y[13],y[17],uherb_fishp,(hsherb_fishp*(thik_s+thik_d))));
 Upt_carn_fishp=         (f1(y[14],y[17],ucarn_fishp,(hscarn_fishp*(thik_s+thik_d))));
 Upt_fishplar_fishp=     (f1(y[18],y[17],ufishplar_fishp,(hsfishplar_fishp*(thik_s+thik_d))));
 Upt_fishdlar_fishp=     (f1(y[20],y[17],ufishdlar_fishp,(hsfishdlar_fishp*(thik_s+thik_d))));
 Upt_herb_fishdlar=      (f1(y[13],y[20],uherb_fishdlar,(hsherb_fishdlar*(thik_s+thik_d))));
 Upt_corpse_fishd=       (f1(y[4],y[19],ucorp_fishd,(hscorp_fishd*(thik_s+thik_d))));
 Upt_disc_fishd=         (f1(y[3],y[19],udisc_fishd,hsdisc_fishd*(thik_s+thik_d)));
 Upt_carn_fishd=         (f1(y[14],y[19],ucarn_fishd,(hscarn_fishd*(thik_s+thik_d))));
 Upt_benths_fishd=       (f1(y[15],y[19],ubenths_fishd,(hsbenths_fishd*(thik_s+thik_d))));
 Upt_benthc_fishd=       (f1(y[16],y[19],ubenthc_fishd,(hsbenthc_fishd*(thik_s+thik_d))));
 Upt_fishplar_fishd=     (f1(y[18],y[19],ufishplar_fishd,(hsfishplar_fishd*(thik_s+thik_d))));
 Upt_fishdlar_fishd=     (f1(y[20],y[19],ufishdlar_fishd,(hsfishdlar_fishd*(thik_s+thik_d))));
 Upt_fishp_fishd=        (f1(y[17],y[19],ufishp_fishd,(hsfishp_fishd*(thik_s+thik_d))));
 Upt_fishd_fishd=        (f1(y[19],y[19],ufishd_fishd,(hsfishd_fishd*(thik_s+thik_d))));
 Upt_corpse_bird=        (f1(y[4],y[21],ucorp_bird,(hscorp_bird*(thik_s+thik_d))));
 Upt_disc_bird=          (f1(y[3],y[21],udisc_bird,(hsdisc_bird*(thik_s+thik_d))));
 Upt_fishp_bird=         (f1(y[17],y[21],ufishp_bird,(hsfishp_bird*(thik_s+thik_d))));
 Upt_fishd_bird=         (f1(y[19],y[21],ufishd_bird,(hsfishd_bird*(thik_s+thik_d))));
 
 
/* _____Ammonia Excretion by each animal consumer group_____ */

 ExcrS_herb=             ((1-aH)*0.5*(Upt_sdetritus_herb + Upt_sphyt_herb));
 ExcrD_herb=             ((1-aH)*0.5*(Upt_ddetritus_herb + Upt_dphyt_herb));
 ExcrS_carn=             ((thik_s/(thik_d+thik_s))*(1-aC)*0.5*(Upt_herb_carn + Upt_fishplar_carn + Upt_fishdlar_carn));
 ExcrD_carn=             ((thik_d/(thik_d+thik_s))*(1-aC)*0.5*(Upt_herb_carn + Upt_fishplar_carn + Upt_fishdlar_carn));
 ExcrD_benths=           ((1-aBs)*0.5*(Upt_ddetritus_benths + Upt_xdetritus_benths + Upt_dphyt_benths));
 ExcrD_benthc=           ((1-aBc)*0.5*(Upt_corpse_benthc + Upt_benths_benthc));
 ExcrS_fishplar=         ((thik_s/(thik_d+thik_s))*(1-aFplar)*0.5*(Upt_herb_fishplar));
 ExcrD_fishplar=         ((thik_d/(thik_d+thik_s))*(1-aFplar)*0.5*(Upt_herb_fishplar));
 ExcrS_fishp=            ((thik_s/(thik_d+thik_s))*(1-aFp)*0.5*(Upt_herb_fishp + Upt_carn_fishp + Upt_fishplar_fishp + Upt_fishdlar_fishp));
 ExcrD_fishp=            ((thik_d/(thik_d+thik_s))*(1-aFp)*0.5*(Upt_herb_fishp + Upt_carn_fishp + Upt_fishplar_fishp + Upt_fishdlar_fishp));
 ExcrS_fishdlar=         ((thik_s/(thik_d+thik_s))*(1-aFdlar)*0.5*(Upt_herb_fishdlar));
 ExcrD_fishdlar=         ((thik_d/(thik_d+thik_s))*(1-aFdlar)*0.5*(Upt_herb_fishdlar));
 ExcrD_fishd=            ((1-aFd)*0.5*(Upt_corpse_fishd + Upt_disc_fishd + Upt_carn_fishd + Upt_benths_fishd + Upt_benthc_fishd + Upt_fishplar_fishd + Upt_fishdlar_fishd + Upt_fishp_fishd + Upt_fishd_fishd));
 ExcrS_bird=             ((1-abird)*0.5*(Upt_corpse_bird + Upt_disc_bird + Upt_fishp_bird + Upt_fishd_bird));


/* _____Defecation rate of each animal consumer group_____ */

 Defec_herb=             ((1-aH)*0.5*(Upt_sdetritus_herb + Upt_sphyt_herb + Upt_ddetritus_herb + Upt_dphyt_herb));
 Defec_carn=             ((1-aC)*0.5*(Upt_herb_carn + Upt_fishplar_carn + Upt_fishdlar_carn));
 Defec_benths=           ((1-aBs)*0.5*(Upt_ddetritus_benths + Upt_xdetritus_benths + Upt_dphyt_benths));
 Defec_benthc=           ((1-aBc)*0.5*(Upt_corpse_benthc + Upt_benths_benthc));
 Defec_fishplar=         ((1-aFplar)*0.5*(Upt_herb_fishplar));
 Defec_fishp=            ((1-aFp)*0.5*(Upt_herb_fishp + Upt_carn_fishp + Upt_fishplar_fishp + Upt_fishdlar_fishp));
 Defec_fishdlar=         ((1-aFdlar)*0.5*(Upt_herb_fishdlar));
 Defec_fishd=            ((1-aFd)*0.5*(Upt_corpse_fishd + Upt_disc_fishd + Upt_carn_fishd + Upt_benths_fishd + Upt_benthc_fishd + Upt_fishplar_fishd + Upt_fishdlar_fishd + Upt_fishp_fishd + Upt_fishd_fishd));
 Defec_bird=             ((1-abird)*0.5*(Upt_corpse_bird + Upt_disc_bird + Upt_fishp_bird + Upt_fishd_bird));

/* _____Assimilation of each animal consumer group_____ */

 Assim_herb=             (aH*(Upt_sdetritus_herb + Upt_sphyt_herb + Upt_ddetritus_herb + Upt_dphyt_herb));
 Assim_carn=             (aC*(Upt_herb_carn + Upt_fishplar_carn + Upt_fishdlar_carn));
 Assim_benths=           (aBs*(Upt_ddetritus_benths + Upt_xdetritus_benths + Upt_dphyt_benths));
 Assim_benthc=           (aBc*(Upt_corpse_benthc + Upt_benths_benthc));
 Assim_fishplar=         (aFplar*(Upt_herb_fishplar));
 Assim_fishp=            (aFp*(Upt_herb_fishp + Upt_carn_fishp + Upt_fishplar_fishp + Upt_fishdlar_fishp));
 Assim_fishdlar=         (aFdlar*(Upt_herb_fishdlar));
 Assim_fishd=            (aFd*(Upt_corpse_fishd + Upt_disc_fishd + Upt_carn_fishd + Upt_benths_fishd + Upt_benthc_fishd + Upt_fishplar_fishd + Upt_fishdlar_fishd + Upt_fishp_fishd + Upt_fishd_fishd));
 Assim_bird=             (abird*(Upt_corpse_bird + Upt_disc_bird + Upt_fishp_bird + Upt_fishd_bird));
 

/* _____Verical mixing fluxes_____ */
// Surface to deep is a negative term.


 Vmix_detritus=          (driverv_dif*60*60*24*((y[1]/thik_d)-(y[0]/thik_s)));
 Vmix_ammonia=           (driverv_dif*60*60*24*((y[6]/thik_d)-(y[5]/thik_s)));
 Vmix_nitrate=           (driverv_dif*60*60*24*((y[9]/thik_d)-(y[8]/thik_s)));
 Vmix_phyt=              (driverv_dif*60*60*24*((y[12]/thik_d)-(y[11]/thik_s)));

/* _____Settling flux  of deep suspended detritus onto the seabed_____ */
// Parameterised to be dependednt on vertcal mixing.
// The sinking rate is scaled by the log-vertical diffusivity on a given day, 
// on the grounds that this is essentially set by the tidal energy dissipation rate.

 detr_settle=            ((dsink_d_Klow-((log10(driverv_dif)-(-6))*((dsink_d_Klow-dsink_d_Khi)/(-3.4-(-6)))))*y[1]);

/* _____Sediment water diffusion fluxes_____ */

 s_w_amm_flx=            (sed_wat_dif*60*60*24*((y[7]/(porosity*thik_x))-(y[6]/thik_d)));
 s_w_nit_flx=            (sed_wat_dif*60*60*24*((y[10]/(porosity*thik_x))-(y[9]/thik_d)));

/* _____Ocean boundary influxes_____ */

 OceanIN_sdetritus=      ((driverboundsdet/thik_s)*drivers_inflow);
 OceanIN_ddetritus=      ((driverboundddet/thik_d)*driverd_inflow);
 OceanIN_sammonia=       ((driverboundsamm/thik_s)*drivers_inflow);
 OceanIN_dammonia=       ((driverbounddamm/thik_d)*driverd_inflow);
 OceanIN_snitrate=       ((driverboundsnit/thik_s)*drivers_inflow);
 OceanIN_dnitrate=       ((driverbounddnit/thik_d)*driverd_inflow);
 OceanIN_sphyt=          ((driverboundsphyt/thik_s)*drivers_inflow);
 OceanIN_dphyt=          ((driverbounddphyt/thik_d)*driverd_inflow);

/*_____Ocean boundary outfluxes_____ */

 OceanOUT_sdetritus=     ((y[0]/thik_s)*(drivers_outflow + driverriver));
 OceanOUT_ddetritus=     ((y[1]/thik_d)*driverd_outflow);
 OceanOUT_sammonia=      ((y[5]/thik_s)*(drivers_outflow + driverriver));
 OceanOUT_dammonia=      ((y[6]/thik_d)*driverd_outflow);
 OceanOUT_snitrate=      ((y[8]/thik_s)*(drivers_outflow + driverriver));
 OceanOUT_dnitrate=      ((y[9]/thik_d)*driverd_outflow);
 OceanOUT_sphyt=         ((y[11]/thik_s)*(drivers_outflow + driverriver));
 OceanOUT_dphyt=         ((y[12]/thik_d)*driverd_outflow);

/* _____Upwelling fluxes_____*/

 Upwelling_det=          ((y[1]/thik_d)*drivers_upwell);
 Upwelling_amm=          ((y[6]/thik_d)*drivers_upwell);
 Upwelling_nit=          ((y[9]/thik_d)*drivers_upwell);
 Upwelling_phyt=         ((y[12]/thik_d)*drivers_upwell);


/* _____River influxes_____*/

 Riv_amm_IN=             ((driverboundriv_amm/thik_s)*driverriver);
 Riv_nit_IN=             ((driverboundriv_nit/thik_s)*driverriver);
 Riv_det_IN=             ((driverboundriv_det/thik_s)*driverriver);

/* _____Atmosphere influxes_____*/

 Atm_amm_IN=             (driveratm_amm);
 Atm_nit_IN=             (driveratm_nit);



/* _____State variable balanced equations_____ */

    // ds_detritus
    ydot[0]= ( xs * y[11] )  
	     - ( m_s * y[0] )  
	     - ( dsink_s * y[0] )  
	     + Vmix_detritus  
	     + OceanIN_sdetritus  
	     + Upwelling_det  
	     + Riv_det_IN  
	     - Upt_sdetritus_herb  
	     - OceanOUT_sdetritus ;

    // dd_detritus
    ydot[1]= Defec_herb  
	     + Defec_carn  
	     + Defec_fishplar  
	     + Defec_fishp  
	     + Defec_fishdlar  
	     + Defec_fishd  
	     + Defec_bird  
	     + ( xd * y[12] ) 
	     + ( dsink_s * y[0] )  
	     - Upt_ddetritus_herb 
	     - Upt_ddetritus_benths 
	     - detr_settle 
	     - ( m_d * y[1] )  
	     - Vmix_detritus  
	     + OceanIN_ddetritus 
	     - Upwelling_det  
	     - OceanOUT_ddetritus ;

    // dx_detritus
    ydot[2]=  Defec_benths 
              + Defec_benthc 
              + detr_settle 
              + ( corp_det * y[4] ) 
              - Upt_xdetritus_benths 
              - ( msed * y[2] ) ;

    // ddiscard
    ydot[3]=  ( Fpdaily * y[17] * Pdiscard )
              + ( Fddaily * y[19] * Ddiscard ) 
              + ( Fsbdaily * y[15] * Sdiscard ) 
              + ( Fcbdaily * y[16] * Sdiscard ) 
              + ( Fcbdaily * demdamage * y[19] ) 
              + ( Fddaily * bendamage * y[15] ) 
              + ( Fddaily * bendamage * y[16] ) 
              - ( disc_corp * y[3] ) 
              - Upt_disc_fishd 
              - Upt_disc_bird ;

    // dcorpse
    ydot[4] = ( disc_corp * y[3] ) 
              + ( xcarn * ( y[14] * y[14] ) ) 
              + ( xbenthc*(y[16]*y[16])) 
              + ( xpfishlar * ( y[18] * y[18] * y[18] ) ) 
              + ( xdfishlar * ( y[20] * y[20] * y[20] ) ) 
              + ( xpfish * ( y[17] * y[17] ) ) 
              + ( xdfish * ( y[19] * y[19] ) ) 
              + ( xbird * ( y[21] * y[21] ) ) 
              - ( corp_det * y[4] ) 
              - Upt_corpse_benthc 
              - Upt_corpse_fishd 
              - Upt_corpse_bird ;

    // ds_ammonia
    ydot[5] = ExcrS_herb 
              + ExcrS_carn 
              + ExcrS_fishplar 
              + ExcrS_fishp 
              + ExcrS_fishdlar 
              + ExcrS_bird 
              + ( m_s * y[0] ) 
              + (( thik_s / ( thik_d + thik_s ) ) * ( ( eH * y[13] ) + ( eC * y[14] ) + ( eFp * y[17] ) + ( eFplar * y[18] ) + ( eFdlar * y[20] ) )) 
              + ( ebird * y[21] ) 
              - ( n_s * y[5] ) 
              - Upt_samm_sphyt 
              + Vmix_ammonia 
              + OceanIN_sammonia 
              + Upwelling_amm 
              + Riv_amm_IN 
              + Atm_amm_IN 
              - OceanOUT_sammonia ;

    // dd_ammonia
    ydot[6] = ExcrD_herb 
              + ExcrD_carn 
              + ExcrD_fishplar 
              + ExcrD_fishp 
              + ExcrD_fishdlar 
              + ExcrD_fishd 
              + ExcrD_benths 
              + ExcrD_benthc 
              + ( m_d * y[1] ) 
              + s_w_amm_flx 
              + ( ( thik_d / ( thik_d + thik_s ) ) * ( ( eH * y[13] ) + ( eC * y[14] ) + ( eFp * y[17] ) + ( eFplar * y[18] ) + ( eFdlar * y[20] ))) 
              + ( eFd * y[19] ) + ( eBs * y[15] ) + ( eBc * y[16] ) 
              - ( n_d * y[6] ) 
              - Vmix_ammonia 
              + OceanIN_dammonia 
              - Upwelling_amm 
              - OceanOUT_dammonia ;
	      
    // dx_ammonia
    ydot[7] = ( msed * y[2] )
              - ( nsed * y[7] )
              - s_w_amm_flx ;
	      
    // ds_nitrate
    ydot[8] = ( n_s * y[5] )
              - ( d_s * y[8] )
              - Upt_snit_sphyt
              + Vmix_nitrate
              + OceanIN_snitrate
              + Upwelling_nit
              + Riv_nit_IN 
              + Atm_nit_IN 
              - OceanOUT_snitrate ;
	      
    // dd_nitrate
    ydot[9] = ( n_d * y[6] )
              - ( d_d * y[9] )
              + s_w_nit_flx
              - Vmix_nitrate 
              + OceanIN_dnitrate
              - Upwelling_nit
              - OceanOUT_dnitrate ;
	      
    // dx_nitrate
    ydot[10] =  ( nsed * y[7] )
              - ( dsed * y[10] )
              - s_w_nit_flx ;
	      
    // ds_phyt
    ydot[11] = Upt_samm_sphyt
              + Upt_snit_sphyt
              - ( xs * y[11] )
              - Upt_sphyt_herb
              + Vmix_phyt
              + OceanIN_sphyt
              + Upwelling_phyt
              - OceanOUT_sphyt ;
	      
    // dd_phyt
    ydot[12] = -Upt_dphyt_herb
              - Upt_dphyt_benths
              - ( xd * y[12] )
              - Vmix_phyt
              + OceanIN_dphyt
              - Upwelling_phyt
              - OceanOUT_dphyt ;
	      
    // dherb
    ydot[13] = Assim_herb
              - ( eH * y[13] )
              - Upt_herb_carn
              - Upt_herb_fishplar
              - Upt_herb_fishdlar
              - Upt_herb_fishp;

    // dcarn
    ydot[14] = Assim_carn
              - ( eC * y[14] )
              - ( xcarn * ( y[14] * y[14] ) )
              - Upt_carn_fishp
              - Upt_carn_fishd ;
	      
    // dbenths
    ydot[15] = Assim_benths
              - ( eBs * y[15] )
              - Upt_benths_fishd
              - Upt_benths_benthc
              - ( Fddaily * bendamage * y[15] )
              - ( Fsbdaily * y[15] ) ;
	      
    // dbenthc
    ydot[16] =  Assim_benthc
              - ( eBc * y[16] )
              - ( xbenthc * ( y[16] * y[16] ) )
              - Upt_benthc_fishd
              - ( Fddaily * bendamage * y[16] )
              - ( Fcbdaily * y[16] ) ;
	      
    // dfishp
    ydot[17] = Assim_fishp
              - ( eFp * y[17] )
              - Upt_fishp_fishd
              - Upt_fishp_bird
              - ( xpfish * ( y[17] * y[17] ) )
              - ( driverpfish_sp * y[17] )
              + ( driverpfish_rec * y[18] )
              - ( Fpdaily*y[17] ) ;
	      
    // dfishplar
    ydot[18] = Assim_fishplar
              - ( eFplar * y[18] )
              - Upt_fishplar_fishp
              - Upt_fishplar_fishd
              - Upt_fishplar_carn
              - ( xpfishlar * ( y[18] * y[18] * y[18] ) )
              + ( driverpfish_sp * y[17] )
              - ( driverpfish_rec * y[18] ) ;
	      
    // dfishd
    ydot[19] = Assim_fishd
              - ( eFd * y[19] )
              - Upt_fishd_fishd
              - Upt_fishd_bird
              - ( xdfish * ( y[19] * y[19] ) )
              - ( driverdfish_sp * y[19] )
              + ( driverdfish_rec * y[20] )
              - ( Fcbdaily*demdamage * y[19] )
              - ( Fddaily * y[19] ) ;
	      
    // dfishdlar
    ydot[20] = Assim_fishdlar
              - ( eFdlar * y[20] )
              - Upt_fishdlar_fishp
              - Upt_fishdlar_fishd
              - Upt_fishdlar_carn
              - ( xdfishlar * ( y[20] * y[20] * y[20] ) )
              + ( driverdfish_sp * y[19] )
              - ( driverdfish_rec * y[20] );
	      
    // dbird
    ydot[21] = Assim_bird
              - ( ebird * y[21] )
              - ( xbird * ( y[21] * y[21] ) ) ;
	      
/* _____Intergrations for the derived variables_____ */

    //dtotpprod
    ydot[22] = Upt_samm_sphyt
	      + Upt_snit_sphyt
              - ( xs * y[11] ) ;

    //dtotnitup              
    ydot[23] = Upt_snit_sphyt ;

    //dtotammup
    ydot[24] = Upt_samm_sphyt ;

    //dphytprod              
    ydot[25] = Upt_samm_sphyt
               + Upt_snit_sphyt ;

    //dherbprod
    ydot[26] = Assim_herb ;

    //dcarnprod              
    ydot[27] = Assim_carn ;

    //dbenthsprod  
    ydot[28] = Assim_benths ;

    //dbenthcprod            
    ydot[29] = Assim_benthc ;

    //dfishplarprod          
    ydot[30] = Assim_fishplar ;

    //dfishpprod             
    ydot[31] = Assim_fishp ;

    //dfishdlarprod          
    ydot[32] = Assim_fishdlar ;

    //dfishdprod             
    ydot[33] = Assim_fishd ;

    //dbirdprod              
    ydot[34] = Assim_bird ;
      
    //dcatchp                
    ydot[35] = Fpdaily * y[17] * ( 1 - Pdiscard ) ;

    //dcatchd                
    ydot[36] = Fddaily * y[19] * ( 1 - Ddiscard ) ;

    //dcatchsb               
    ydot[37] = Fsbdaily * y[15] * ( 1 - Sdiscard ) ;

    //dcatchcb               
    ydot[38] = Fcbdaily * y[16] * ( 1 - Sdiscard ) ;

    //dwcmineral             
    ydot[39] = ( m_s * y[0] ) + ( m_d * y[1] ) ;

    //dsedmineral            
    ydot[40] = msed * y[2] ;

    //dwcnitrif              
    ydot[41] = ( n_s * y[5] ) + ( n_d * y[6] ) ;

    //dsednitrif             
    ydot[42] = nsed * y[7] ;

    //dwcdenitrif            
    ydot[43] = ( d_s * y[8] ) + ( d_d * y[9] ) ;

    //dseddenitrif           
    ydot[44] = dsed * y[10] ;

    //dwcammprod            
    ydot[45] = ExcrS_herb
               + ExcrS_carn
               + ExcrS_fishplar
               + ExcrS_fishp
               + ExcrS_fishdlar
               + ExcrS_bird
               + ExcrD_herb
               + ExcrD_carn
               + ExcrD_fishplar
               + ExcrD_fishp
               + ExcrD_fishdlar
               + ExcrD_fishd
               + ( m_s * y[0] )
               + ( m_d * y[1] )
               + ( eH * y[13] ) 
               + ( eC * y[14] ) 
               + ( eFplar * y[18] ) 
               + ( eFp * y[17] ) 
               + ( eFdlar * y[20] ) 
               + ( eFd * y[19] )
               + ( ebird * y[21] ) ;

    //dsedammprod            
    ydot[46] = ExcrD_benths
               + ExcrD_benthc
               + ( eBs * y[15] )
               + ( eBc * y[16] )
               + s_w_amm_flx ;
	       
// Excretion by benthos carnivores and susp feeders goes straight to deep water layer so their excretion is included
// in the sediment water flux 

    //dwcnitprod             
    ydot[47] = ( n_s * y[5] ) + ( n_d * y[6] ) ;

    //dsednitprod            
    ydot[48] = s_w_nit_flx ;

    //dfluxherb_carn         
    ydot[49] = Upt_herb_carn ;

    //dfluxherb_pfishlar     
    ydot[50] = Upt_herb_fishplar ;

    //dfluxherb_dfishlar     
    ydot[51] = Upt_herb_fishdlar ;

    //dfluxherb_pfish        
    ydot[52] = Upt_herb_fishp ;

    //dfluxcarn_pfish        
    ydot[53] = Upt_carn_fishp ;

    //dfluxcarn_dfish        
    ydot[54] = Upt_carn_fishd ;

    //dfluxpfishlar_carn     
    ydot[55] = Upt_fishplar_carn ;

    //dfluxpfishlar_pfish    
    ydot[56] = Upt_fishplar_fishp ;

    //dfluxpfishlar_dfish    
    ydot[57] = Upt_fishplar_fishd ;

    //dfluxpfish_dfish       
    ydot[58] = Upt_fishp_fishd ;

    //dfluxpfish_bird        
    ydot[59] = Upt_fishp_bird ;

    //dfluxbens_dfish        
    ydot[60] = Upt_benths_fishd ;

    //dfluxbens_benc         
    ydot[61] = Upt_benths_benthc ;

    //dfluxbenc_dfish 
    ydot[62] = Upt_benthc_fishd ;

    //dfluxdfishlar_carn     
    ydot[63] = Upt_fishdlar_carn ;

    //dfluxdfishlar_pfish   
    ydot[64] = Upt_fishdlar_fishp ;

    //dfluxdfishlar_dfish    
    ydot[65] = Upt_fishdlar_fishd ;

    //dfluxdfish_dfish       
    ydot[66] = Upt_fishd_fishd ;

    //dfluxdfish_bird        
    ydot[67] = Upt_fishd_bird ;

    //dfluxdisc_dfish        
    ydot[68] = Upt_disc_fishd ;

    //dfluxdisc_bird         
    ydot[69] = Upt_disc_bird ;

    //dfluxcorp_benthc       
    ydot[70] = Upt_corpse_benthc ;

    //dfluxcorp_dfish        
    ydot[71] = Upt_corpse_fishd ;

    //dfluxcorp_bird         
    ydot[72] = Upt_corpse_bird ;

    //dfluxDINinflow         
    ydot[73] = OceanIN_sammonia 
	      + OceanIN_dammonia 
	      + OceanIN_snitrate 
	      + OceanIN_dnitrate ;

    //dfluxDINoutflow        
    ydot[74] = OceanOUT_sammonia 
	      + OceanOUT_dammonia 
	      + OceanOUT_snitrate 
	      + OceanOUT_dnitrate ;

    //dfluxPARTinflow        
    ydot[75] = OceanIN_sphyt 
	      + OceanIN_dphyt 
	      + OceanIN_sdetritus 
	      + OceanIN_ddetritus ;

// NB. THE INPUT OF DETRITUS BY RIVERS Is EXCLUDED HERE - ITS ONLY THE OCEAN INPUT
//                       + Riv_det_IN

    //dfluxPARToutflow        
    ydot[76] = OceanOUT_sphyt 
	      + OceanOUT_dphyt 
	      + OceanOUT_sdetritus 
	      + OceanOUT_ddetritus ;

    //datmosDINinput         
    ydot[77] = Atm_amm_IN + Atm_nit_IN ;

    //drivDINinflow          
    ydot[78] = Riv_amm_IN + Riv_nit_IN ;

    //drivPARTinflow         
    ydot[79] = Riv_det_IN ;

    //dvertnitflux           
    ydot[80] = Vmix_nitrate + Upwelling_nit ;
                
    //dhoriznitflux        
    ydot[81] = Riv_nit_IN
               + Atm_nit_IN 
               + OceanIN_snitrate
               - OceanOUT_snitrate ;

    //dPNP        
    ydot[82] = - ( n_s * y[5] )
               + ( d_s * y[8] )
               + Upt_snit_sphyt ;

    //dPfish_spawn           
    ydot[83] = driverpfish_sp * y[17] ;

    //dPfish_recruit         
    ydot[84] = driverpfish_rec * y[18] ;

    //dDfish_spawn           
    ydot[85] = driverdfish_sp * y[19] ;

    //dDfish_recruit         
    ydot[86] = driverdfish_rec * y[20] ;

    //ddiscpel               
    ydot[87] = Fpdaily * y[17] * ( Pdiscard ) ;

    //ddiscdem               
    ydot[88] = ( Fddaily * y[19] * ( ( Ddiscard ) ) ) + ( Fcbdaily * demdamage * y[19] ) ;

    //ddiscsfb               
    ydot[89] = ( Fsbdaily * y[15] * ( Sdiscard ) ) + ( Fddaily * bendamage * y[15] ) ;

    //ddisccfb               
    ydot[90] = ( Fcbdaily * y[16] * ( Sdiscard ) ) + ( Fddaily * bendamage * y[15] ) ;

    //dfluxpartwc_sed        
    ydot[91] = Upt_dphyt_benths
               + Upt_ddetritus_benths
               + detr_settle
               + ( disc_corp * y[3] )
               + xcarn * ( y[14] * y[14] )
               + xpfishlar * ( y[18] * y[18] * y[18] )
               + xdfishlar * ( y[20] * y[20] * y[20] )
               + xpfish * ( y[17] * y[17] )
               + xdfish * ( y[19] * y[19] )
               + xbird * ( y[21] * y[21] ) ;
	       
// Includes the grazing flux of suspended detritus and phytoplankton to benths

    //dfluxdisc_sed          
    ydot[92] = disc_corp * y[3] ;
    



}

/* END file E2E_Foodweb.c */
