#Print some of the full time series data to a csv file
#-----------------------------------------------------------------

tso<-cbind(out$s_nitrate,out$d_nitrate,out$x_nitrate,out$s_ammonia,out$d_ammonia,out$x_ammonia,out$s_detritus,out$d_detritus,out$x_detritus,((((out$s_phyt))*12*106)/16)/40,((((out$d_phyt))*12*106)/16)/40)
colnames(tso)<-c("surfnitrate/m2","deepnitrate/m2","sednitrate/m2sed","surfammonia/m2","deepammonia/m2","sedammonia/m2sed","surfdetritus/m2","deepdetritus/m2","seddetritus/m2sed","surfchlmg/m2","deepchlmg/m2")
write.table(tso,file=paste(oudir,"model_runresults_nutrients_depthintegrated",AAA,".csv",sep=""),sep=",",row.names=FALSE)


