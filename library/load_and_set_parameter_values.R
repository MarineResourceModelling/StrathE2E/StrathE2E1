# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Subroutine to load and set the parameter values to be use din the model run
#
# Parameters which have been established by the simulated annealing scheme are
# loaded from a csv file which is made from the last line of the 'accepted parameters'
# file produced by the annealing programme.
#
# A variety of other parameters which are not optimised by simulated annealing are
# hard-wired in this subroutine
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#Various values are hard-wired here...
#These are:

#irradiance at maximum nutrient uptake by phytoplankton
            satlight<-5.0

#Autotroph Q10 value
            qtenauto<-2.0

#Heterotroph uptake Q10 value
            qtenhetero<-2.2

#Metabolic and bacterial Q10 value
            qtenmetabol<-2.4

#Q10 reference temperature
            qtenrreft<-10


#Proportion of consumption converted to growth - temperature independent
#                              zooplankton, carnivores,
#                              susp_benthos, carn_benthos,
#                              pel_fash_larvae, dem_fish_larvae,
#                              pelagic_fish, demersal_fish,
#                              birds&mammals
            asimH<-0.34
            asimC<-0.34
            asimBs<-0.34
            asimBc<-0.34
            asimFplar<-0.34
            asimFdlar<-0.34
            asimFp<-0.275
            asimFd<-0.25
            asimbird<-0.15


#Background proportion of biomass converted to ammonia per day at the reference temperature (birds and mammals T independent)
#                              zooplankton, carnivores,
#                              susp_benthos, carn_benthos,
#                              pel_fash_larvae, dem_fish_larvae,
#                              pelagic_fish, demersal_fish,
#                              birds&mammals
            excrHt<-0.01
            excrCt<-0.005
            excrBst<-0.01
            excrBct<-0.0075
            excrFplart<-0.00005
            excrFdlart<-0.00005
            excrFpt<-0.001
            excrFdt<-0.001
#            excrbird<-0.0005
#            excrbird<-0.000275753
            excrbird<-0.000275740    # hand refined value to give stationary model with default drivers

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



#Now read in the externally stored parameters

datastore<-read.csv(parameterfile,header=TRUE)


PREF_NIT_phyt<-datastore$PREF_NIT_phyt[nrow(datastore)]
PREF_AMM_phyt<-datastore$PREF_AMM_phyt[nrow(datastore)]

PREF_phyt_herb<-datastore$PREF_phyt_herb[nrow(datastore)]
PREF_det_herb<-datastore$PREF_det_herb[nrow(datastore)]

PREF_herb_carn<-datastore$PREF_herb_carn[nrow(datastore)]
PREF_fishplar_carn<-datastore$PREF_fishplar_carn[nrow(datastore)]
PREF_fishdlar_carn<-datastore$PREF_fishdlar_carn[nrow(datastore)]

PREF_herb_fishplar<-datastore$PREF_herb_fishplar[nrow(datastore)]
               
PREF_herb_fishp<-datastore$PREF_herb_fishp[nrow(datastore)]
PREF_carn_fishp<-datastore$PREF_carn_fishp[nrow(datastore)]
PREF_fishdlar_fishp<-datastore$PREF_fishdlar_fishp[nrow(datastore)]
PREF_fishplar_fishp<-datastore$PREF_fishplar_fishp[nrow(datastore)]

PREF_herb_fishdlar<-datastore$PREF_herb_fishdlar[nrow(datastore)]

PREF_carn_fishd<-datastore$PREF_carn_fishd[nrow(datastore)]
PREF_benths_fishd<-datastore$PREF_benths_fishd[nrow(datastore)]
PREF_benthc_fishd<-datastore$PREF_benthc_fishd[nrow(datastore)]
PREF_fishplar_fishd<-datastore$PREF_fishplar_fishd[nrow(datastore)]
PREF_fishdlar_fishd<-datastore$PREF_fishdlar_fishd[nrow(datastore)]
PREF_fishp_fishd<-datastore$PREF_fishp_fishd[nrow(datastore)]
PREF_fishd_fishd<-datastore$PREF_fishd_fishd[nrow(datastore)]
PREF_disc_fishd<-datastore$PREF_disc_fishd[nrow(datastore)]
PREF_corp_fishd<-datastore$PREF_corp_fishd[nrow(datastore)]

PREF_phyt_benths<-datastore$PREF_phyt_benths[nrow(datastore)]
PREF_det_benths<-datastore$PREF_det_benths[nrow(datastore)]
PREF_sed_benths<-datastore$PREF_sed_benths[nrow(datastore)]

PREF_benths_benthc<-datastore$PREF_benths_benthc[nrow(datastore)]
PREF_benthd_benthc<-datastore$PREF_benthd_benthc[nrow(datastore)]
PREF_corp_benthc<-datastore$PREF_corp_benthc[nrow(datastore)]

PREF_fishp_bird<-datastore$PREF_fishp_bird[nrow(datastore)]
PREF_fishd_bird<-datastore$PREF_fishd_bird[nrow(datastore)]
PREF_disc_bird<-datastore$PREF_disc_bird[nrow(datastore)]
PREF_corp_bird<-datastore$PREF_corp_bird[nrow(datastore)]


#Then we set the rate parameters for each predator at the reference temperature

u_phyt<-datastore$u_phyt[nrow(datastore)]
u_herb<-datastore$u_herb[nrow(datastore)]
u_carn<-datastore$u_carn[nrow(datastore)]
u_fishplar<-datastore$u_fishplar[nrow(datastore)]
u_fishp<-datastore$u_fishp[nrow(datastore)]
u_fishdlar<-datastore$u_fishdlar[nrow(datastore)]
u_fishd<-datastore$u_fishd[nrow(datastore)]
u_benths<-datastore$u_benths[nrow(datastore)]
u_benthc<-datastore$u_benthc[nrow(datastore)]
u_bird<-datastore$u_bird[nrow(datastore)]

h_phyt<-datastore$h_phyt[nrow(datastore)]
h_herb<-datastore$h_herb[nrow(datastore)]
h_carn<-datastore$h_carn[nrow(datastore)]
h_fishplar<-datastore$h_fishplar[nrow(datastore)]
h_fishp<-datastore$h_fishp[nrow(datastore)]
h_fishdlar<-datastore$h_fishdlar[nrow(datastore)]
h_fishd<-datastore$h_fishd[nrow(datastore)]
h_benths<-datastore$h_benths[nrow(datastore)]
h_benthc<-datastore$h_benthc[nrow(datastore)]
h_bird<-datastore$h_bird[nrow(datastore)]


#Mineralisation, nitrification and denitrification rates per day at the reference temperature
         xmt<-datastore$xmt[nrow(datastore)]
         xnst<-datastore$xnst[nrow(datastore)]
         xdst<-datastore$xdst[nrow(datastore)]
         xndt<-datastore$xndt[nrow(datastore)]
         xddt<-datastore$xddt[nrow(datastore)]
         xmsedt<-datastore$xmsedt[nrow(datastore)]
         xnsedt<-datastore$xnsedt[nrow(datastore)]
         xdsedt<-datastore$xdsedt[nrow(datastore)]
#Proportion of discards sinking to become seabed corpses per day - temperature independent
         xdisc_corp<-datastore$xdisc_corp[nrow(datastore)]
#Proportion of corpse mass converted to detritus per day at the reference temperature
         xxcorp_det<-datastore$xcorp_det[nrow(datastore)]


#Sinking rates and their dependence on mixing - temperature independent
         xdsink_s<-datastore$xdsink_s[nrow(datastore)]
         xdsink_d_Klow<-datastore$xdsink_d_Klow[nrow(datastore)]
         xdsink_d_Khi<-datastore$xdsink_d_Khi[nrow(datastore)]

#Death rates of phytoplankton at the reference temperature
         xxst<-datastore$xxst[nrow(datastore)]
         xxdt<-datastore$xxdt[nrow(datastore)]
#Death rate of carnivores fish birds and mammals per unit biomass - temperature independent
         xxcarn<-datastore$xxcarn[nrow(datastore)]
         xxbenthc<-datastore$xxbenthc[nrow(datastore)]
         xxpfishlar<-datastore$xxpfishlar[nrow(datastore)]
         xxdfishlar<-datastore$xxdfishlar[nrow(datastore)]
         xxpfish<-datastore$xxpfish[nrow(datastore)]
         xxdfish<-datastore$xxdfish[nrow(datastore)]
         xxbird<-datastore$xxbird[nrow(datastore)]

#Density dependent discarding coefficient for demersal fish
         xdfdp<-datastore$xdfdp[nrow(datastore)]


#End of external import of parameters
#----------------------------------------------------------------------------


#--------------------------------------------------------------------------
#Load all the parameter values into a single vector which will be passed to the model function. 


parms<-c(thik_s=s_depth,thik_d=d_depth,thik_x=x_depth,porosity=x_poros,
         thik_b=bx_depth,
         Lmaxup=satlight,
         sed_wat_dif=(10^Kxw),

#Q10 temperature coefficients for heteroptophs, autotrophs and reference temperature
         qtena=qtenauto,
         qtenh=qtenhetero,
         qtenm=qtenmetabol,
         qtenr=qtenrreft,

#Preferences of zooplankton for phytoplankton and detritus - used to drive their impled vertical distribution
         p_pref=PREF_phyt_herb,
         d_pref=PREF_det_herb,

#Nutrient uptake by phytoplankton at the reference temperature
         uNIT_phytt=u_phyt*PREF_NIT_phyt,hsNIT_phyt=h_phyt,
         uAMM_phytt=u_phyt*PREF_AMM_phyt,hsAMM_phyt=h_phyt,

#Feeding by mesozooplankton at the reference temperature
         uphyt_herbt=u_herb*PREF_phyt_herb,hsphyt_herb=h_herb,
         udet_herbt=u_herb*PREF_det_herb,hsdet_herb=h_herb,

#Feeding by carnivorous zooplankton at the reference temperature
         uherb_carnt=u_carn*PREF_herb_carn,hsherb_carn=h_carn,
         ufishplar_carnt=u_carn*PREF_fishplar_carn,hsfishplar_carn=h_carn,
         ufishdlar_carnt=u_carn*PREF_fishdlar_carn,hsfishdlar_carn=h_carn,

#Feeding by larvae of pelagic fish at the reference temperature
         uherb_fishplart=u_fishplar*PREF_herb_fishplar,hsherb_fishplar=h_fishplar,

#Feeding by pelagic fish at the reference temperature
         uherb_fishpt=u_fishp*PREF_herb_fishp,hsherb_fishp=h_fishp,
         ucarn_fishpt=u_fishp*PREF_carn_fishp,hscarn_fishp=h_fishp,
         ufishdlar_fishpt=u_fishp*PREF_fishdlar_fishp,hsfishdlar_fishp=h_fishp,
         ufishplar_fishpt=u_fishp*PREF_fishplar_fishp,hsfishplar_fishp=h_fishp,

#Feeding by larvae of demersal fish at the reference temperature
         uherb_fishdlart=u_fishdlar*PREF_herb_fishdlar,hsherb_fishdlar=h_fishdlar,

#Feeding by demersal fish at the reference temperature
         ucarn_fishdt=u_fishd*PREF_carn_fishd,hscarn_fishd=h_fishd,
         ubenths_fishdt=u_fishd*PREF_benths_fishd,hsbenths_fishd=h_fishd,
         ubenthc_fishdt=u_fishd*PREF_benthc_fishd,hsbenthc_fishd=h_fishd,
         ufishplar_fishdt=u_fishd*PREF_fishplar_fishd,hsfishplar_fishd=h_fishd,
         ufishdlar_fishdt=u_fishd*PREF_fishdlar_fishd,hsfishdlar_fishd=h_fishd,
         ufishp_fishdt=u_fishd*PREF_fishp_fishd,hsfishp_fishd=h_fishd,
         ufishd_fishdt=u_fishd*PREF_fishd_fishd,hsfishd_fishd=h_fishd,
         udisc_fishdt=u_fishd*PREF_disc_fishd,hsdisc_fishd=h_fishd,
         ucorp_fishdt=u_fishd*PREF_corp_fishd,hscorp_fishd=h_fishd,

#Feeding by suspension feeding benthos at the reference temperature
         uphyt_benthst=u_benths*PREF_phyt_benths,hsphyt_benths=h_benths,
         udet_benthst=u_benths*PREF_det_benths,hsdet_benths=h_benths,
         used_benthst=u_benths*PREF_sed_benths,hssed_benths=h_benths,

#Feeding by carnivorous benthos at the reference temperature
         ubenths_benthct=u_benthc*PREF_benths_benthc,hsbenths_benthc=h_benthc,
         ucorp_benthct=u_benthc*PREF_corp_benthc,hscorp_benthc=h_benthc,

#Feeding by birds and mammals - temperature independent
         ufishp_bird=u_bird*PREF_fishp_bird,hsfishp_bird=h_bird,
         ufishd_bird=u_bird*PREF_fishd_bird,hsfishd_bird=h_bird,
         udisc_bird=u_bird*PREF_disc_bird,hsdisc_bird=h_bird,
         ucorp_bird=u_bird*PREF_corp_bird,hscorp_bird=h_bird,

#Proportion of consumption converted to growth - temperature independent
         aH=asimH,aC=asimC,
         aBs=asimBs,aBc=asimBc,
         aFplar=asimFplar,aFdlar=asimFdlar,
         aFp=asimFp,aFd=asimFd,
         abird=asimbird,

#Background proportion of biomass converted to ammonia per day at the reference temperature (birds and mammals T independent)
         eHt=excrHt,eCt=excrCt,
         eBst=excrBst,eBct=excrBct,
         eFplart=excrFplart,eFdlart=excrFdlart,
         eFpt=excrFpt,eFdt=excrFdt,
         ebird=excrbird,

#Mineralisation, nitrification and denitrification rates per day at the reference temperature
         mt=xmt,
         nst=xnst,dst=xdst,
         ndt=xndt,ddt=xddt,
         msedt=xmsedt,nsedt=xnsedt,dsedt=xdsedt,

#Death rates of phytoplankton at the reference temperature
         xst=xxst,xdt=xxdt,

#Death rate per unit biomass for carnivores fish birds and mammals
        xcarn=xxcarn,
        xbenthc=xxbenthc,
        xpfishlar=xxpfishlar,
        xdfishlar=xxdfishlar,
        xpfish=xxpfish,
        xdfish=xxdfish,
        xbird=xxbird,

#Sinking rates and their dependence on mixing - temperature independent
         dsink_s=xdsink_s,dsink_d_Klow=xdsink_d_Klow,dsink_d_Khi=xdsink_d_Khi,

#Fishing proportion of biomass removed per day - temperature independent
#         Fpdaily=PFrate,Fddaily=DFrate,Fsbdaily=SBFrate,Fcbdaily=CBFrate,
#Not needed as fixed parameters - now included as timeseries functions

#Discarding rate for pelagic demersal and shellfish
         Pdiscard=Pelagicdiscard,
         #Ddiscard=Demersaldiscard, # Not needed
         Sdiscard=Shellfishdiscard,
#Coefficient for density dependent discarding of demersal fish
         dfdp=xdfdp,
#Proportion of demersal fishing rate inflicted as collateral mortality on benthos
#100% of this mortality goes to corpses
         bendamage=benthosdamage,
#Proportion of carnovorous benthos fishing rate inflicted as collateral mortality on demersal fish
#100% of this mortality goes to corpses
         demdamage=demersaldamage,

#Proportion of discarded fish sinking to become seabed corpses per day
         disc_corp=xdisc_corp,

#Proportion of seabed corpses becoming seabed detritus per day
         xcorp_det=xxcorp_det

)


#-------------------------------------------------------------------------------------------------------


