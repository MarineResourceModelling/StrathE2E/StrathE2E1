#Extract the ratios of all state varibales to s_nitrate atthe end of the run
#to be used as ijitial conditions at the strat of a new run
#Print to a csv file
#-----------------------------------------------------------------


e_s_nitr<-out$s_nitrate[nrow(out)]/s_depth
e_d_nitr<-out$d_nitrate[nrow(out)]/d_depth
e_x_nitr<-out$x_nitrate[nrow(out)]/x_depth

ir1<-e_s_nitr/e_s_nitr
ir2<-e_d_nitr/e_s_nitr
ir3<-e_x_nitr/e_s_nitr

ir4<-out$s_ammonia[nrow(out)]/out$s_nitrate[nrow(out)]
ir5<-out$d_ammonia[nrow(out)]/out$d_nitrate[nrow(out)]
ir6<-out$x_ammonia[nrow(out)]/out$x_nitrate[nrow(out)]

ir7<-out$s_detritus[nrow(out)]/out$s_nitrate[nrow(out)]
ir8<-out$d_detritus[nrow(out)]/out$d_nitrate[nrow(out)]
ir9<-out$x_detritus[nrow(out)]/out$x_nitrate[nrow(out)]

#ir10<-out$s_flag[nrow(out)]/out$s_nitrate[nrow(out)]
#ir11<-out$d_flag[nrow(out)]/out$s_nitrate[nrow(out)]

ir12<-out$s_phyt[nrow(out)]/out$s_nitrate[nrow(out)]
ir13<-out$d_phyt[nrow(out)]/out$s_nitrate[nrow(out)]

#ir14<-out$s_uzoo[nrow(out)]/out$s_nitrate[nrow(out)]
#ir15<-out$d_uzoo[nrow(out)]/out$s_nitrate[nrow(out)]

ir16<-out$herb[nrow(out)]/out$s_nitrate[nrow(out)]
ir17<-out$carn[nrow(out)]/out$s_nitrate[nrow(out)]

ir18<-out$benths[nrow(out)]/out$s_nitrate[nrow(out)]
#ir19<-out$benthd[nrow(out)]/out$s_nitrate[nrow(out)]
ir20<-out$benthc[nrow(out)]/out$s_nitrate[nrow(out)]

ir21<-out$fishp[nrow(out)]/out$s_nitrate[nrow(out)]
ir22<-out$fishplar[nrow(out)]/out$s_nitrate[nrow(out)]

ir23<-out$fishd[nrow(out)]/out$s_nitrate[nrow(out)]
ir24<-out$fishdlar[nrow(out)]/out$s_nitrate[nrow(out)]

ir25<-out$bird[nrow(out)]/out$s_nitrate[nrow(out)]

ir26<-out$discard[nrow(out)]/out$s_nitrate[nrow(out)]

ir27<-out$corpse[nrow(out)]/out$s_nitrate[nrow(out)]


#irall<-rbind(e_s_nitr,ir1,ir2,ir3,ir4,ir5,ir6,ir7,ir8,ir9,ir10,ir11,ir12,ir13,ir14,ir15,ir16,ir17,ir18,ir19,ir20,ir21,ir22,ir23,ir24,ir25,ir26)
#rownames(irall)<-c("surfnitrate/m3","surfnitR","deepnitR","sednitR","surfammR","deepammR","sedammR","surfdetR","deepdetR","seddetR","surfflagR","deepflagR","surfphytR","deepphytR","surfuzooR","deepuzooR","herbR","carnR","bensR","bendR","bencR","pfishR","pfishlarR","dfishR","dfishlarR","birdR","discR")
#write.table(irall,file=paste(oudir,"model_endstate_export",AAA,".csv",sep=""),sep=",",col.names=FALSE)

irall<-rbind(e_s_nitr,ir1,ir2,ir3,ir4,ir5,ir6,ir7,ir8,ir9,ir12,ir13,ir16,ir17,ir18,ir20,ir21,ir22,ir23,ir24,ir25,ir26,ir27)
rownames(irall)<-c("surfnitrate/m3","surfnitR","deepnitR","sednitR","surfammR","deepammR","sedammR","surfdetR","deepdetR","seddetR","surfphytR","deepphytR","herbR","carnR","bensR","bencR","pfishR","pfishlarR","dfishR","dfishlarR","birdR","discR","corpseR")
write.table(irall,file=paste(oudir,"model_endstate_export",AAA,".csv",sep=""),sep=",",col.names=FALSE)

